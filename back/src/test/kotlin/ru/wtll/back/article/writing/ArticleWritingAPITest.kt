package ru.wtll.back.article.writing

import TestCommon
import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.http.HttpStatus
import org.springframework.mock.web.MockHttpServletRequest
import org.springframework.mock.web.MockMultipartFile
import org.springframework.test.context.junit.jupiter.SpringExtension
import ru.wtll.back.article.Article
import ru.wtll.back.article.ArticleConfig
import ru.wtll.back.article.components.ArticleTextBlock
import java.io.FileReader
import javax.servlet.http.Cookie

//@SpringBootTest
//@ExtendWith(SpringExtension::class)
internal class ArticleWritingAPITest {

    val article = Article(
            ArticleConfig("somearticle", "preview"),
            arrayOf(ArticleTextBlock("lalal", ArticleTextBlock.TextConfig()))
    )

    final val articlesDir = TestCommon.testsFolder.toPath().resolve("writingapi")

    val api = ArticleWritingAPI(articlesDir.toFile(), false)

    val multipartFile = MockMultipartFile("somefile.png", "TestingExample".toByteArray())

    private val authorCookie = Cookie("id", "1")


//    @BeforeEach
//    fun aa() {
//        println(api.dbAPI)
//    }

    @Test
    fun saveArticle() {

        val request = MockHttpServletRequest("post", "/api/article/save")
        request.setCookies(authorCookie)

        val result = api.saveArticle(request, article)

        assertEquals(HttpStatus.CREATED, result.statusCode)
        assertEquals(article,
                Article.read(
                        articlesDir.resolve(article.config.code).toFile(),
                        article.config.state.toString()
                )
        )
    }

    @Test
    fun deleteArticle() {
        saveArticle()

        val request = MockHttpServletRequest("post", "/api/article/delete")
        request.setCookies(authorCookie)

        val deleteRes = api.deleteArticle(request, article.config.name)
        assertEquals(HttpStatus.OK, deleteRes.statusCode)
    }

    @Test
    fun saveFile() {

        val request = MockHttpServletRequest("post", "/api/article/save_file")
        request.setCookies(authorCookie)

        val result = api.saveFile(request, article.config.name, multipartFile)
        assertEquals(HttpStatus.CREATED, result.statusCode)
        assertNotNull(result.body)

        val written = articlesDir.resolve(article.config.code).resolve("files").resolve(result.body!!)
        var bytesFromFile: ByteArray? = null
        FileReader(written.toFile()).use {
            bytesFromFile = it.readText().toByteArray()
        }

        assertNotNull(bytesFromFile)
        assertTrue(multipartFile.bytes.contentEquals(bytesFromFile!!))
    }
}