package ru.wtll.back.article.reading

import TestCommon
import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.Test
import org.springframework.http.HttpStatus
import org.springframework.mock.web.MockHttpServletRequest
import ru.wtll.back.article.writing.ArticleWritingAPITest
import javax.servlet.http.Cookie

internal class ArticleReadingAPITest {

    val writingTest = ArticleWritingAPITest()
    val readingApi = ArticleReadingAPI(writingTest.articlesDir.toFile())

    private val authorCookie = Cookie("id", "1")

    @Test
    fun getArticle() {
        TestCommon.testsFolder.deleteRecursively()
        TestCommon.testsFolder.mkdirs()


        var readResult = readingApi.getArticle(
                writingTest.article.config.code,
                writingTest.article.config.state.toString(),
                true
        )
        assertEquals(HttpStatus.NOT_FOUND, readResult.statusCode)

        writingTest.saveArticle()
        readResult = readingApi.getArticle(
                writingTest.article.config.code,
                writingTest.article.config.state.toString(),
                true
        )

        assertEquals(HttpStatus.OK, readResult.statusCode)
        assertEquals(writingTest.article, readResult.body)
    }

    @Test
    fun getFile() {

        val request = MockHttpServletRequest("post", "/api/article/save_file")
        request.setCookies(authorCookie)


        val writeResult = writingTest.api.saveFile(request, writingTest.article.config.name, writingTest.multipartFile)

        assertNotNull(writeResult.body)

        val readResult = readingApi.getFile(writingTest.article.config.code, writeResult.body!!)

        val content = readResult.body?.inputStream?.readBytes()

        assertEquals(HttpStatus.OK, readResult.statusCode)
        assertNotNull(content)
        assertTrue(writingTest.multipartFile.bytes.contentEquals(content!!))
    }
}