package ru.wtll.back.article

import TestCommon
import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.Test
import ru.wtll.back.article.components.ArticleAudioBlock
import ru.wtll.back.article.components.ArticleImageBlock
import ru.wtll.back.article.components.ArticleTextBlock

internal class ArticleTest {

    @Test
    fun containsText() {
        val article = Article(
                ArticleConfig("Title", "preview"),
                arrayOf(
                        ArticleTextBlock("test",
                                ArticleTextBlock.TextConfig(
                                        blockType = ArticleTextBlock.TextConfig.BlockType.SUBTITLE)),

                        ArticleAudioBlock().apply {
                            audios.addAll(listOf(ArticleAudioBlock.Audio("audiopath", "audioname")))
                        },

                        ArticleTextBlock("another test",
                                ArticleTextBlock.TextConfig(
                                        blockType = ArticleTextBlock.TextConfig.BlockType.SIDEBAR)),

                        ArticleImageBlock().apply {
                            images.addAll(listOf(ArticleImageBlock.Image("someimage", "why we need communism")))
                        }
                ),
                false
        )


        assertTrue(article.containsText("test"))
        assertTrue(article.containsText("TeSt"))
        assertTrue(article.containsText("auDioNAme"))
        assertTrue(article.containsText("another"))
        assertTrue(article.containsText("coMMunism"))
        assertFalse(article.containsText("Title"))
        assertFalse(article.containsText("audiopath"))
        assertFalse(article.containsText("someimage"))
        assertFalse(article.containsText("aaaaaaaaaaaaaaaa"))
    }

    @Test
    fun write() {
        val article = Article(
                ArticleConfig("Title", "preview"),
                arrayOf(
                        ArticleTextBlock("test",
                                ArticleTextBlock.TextConfig(
                                        blockType = ArticleTextBlock.TextConfig.BlockType.SUBTITLE)),

                        ArticleAudioBlock().apply {
                            audios.addAll(listOf(ArticleAudioBlock.Audio("audiopath", "audioname")))
                        },

                        ArticleTextBlock("another test",
                                ArticleTextBlock.TextConfig(
                                        blockType = ArticleTextBlock.TextConfig.BlockType.SIDEBAR)),

                        ArticleImageBlock().apply {
                            images.addAll(listOf(ArticleImageBlock.Image("someimage", "why we need communism")))
                        }
                ),
                true
        )

        val articleDir = TestCommon.testsFolder.toPath().resolve("arl_test").resolve(article.config.state.toString()).toFile()

        article.write(articleDir, false, TestCommon.testsFolder)

        assertEquals(article, Article.read(articleDir.parentFile, article.config.state.toString(), true))

    }

}