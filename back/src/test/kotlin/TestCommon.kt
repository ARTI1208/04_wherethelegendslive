import java.nio.file.Paths

object TestCommon {

    val testsFolder = Paths.get("build", "unit_testing").toFile()

    init {
        testsFolder.mkdirs()
    }

}