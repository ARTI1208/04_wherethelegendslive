package ru.wtll.back.users;

public interface UserDataAccess {

    boolean getUserExist(String email);

    User getUserByEmail(String email);

    void insert(User user);
}
