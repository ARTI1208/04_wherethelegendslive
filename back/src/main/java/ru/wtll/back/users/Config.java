package ru.wtll.back.users;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.jdbc.datasource.DriverManagerDataSource;

import java.io.IOException;

@Configuration
public class Config {
    @Bean
    DriverManagerDataSource dataSource() {
        return new DriverManagerDataSource(System.getenv("WTLL_DB_URL"),
                System.getenv("WTLL_DB_USER_NAME"),
                System.getenv("WTLL_DB_USER_PASSWORD"));
    }



}
