package ru.wtll.back.users;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public interface GenerateKeyDAO {
    @GetMapping("/key/generate_key")
    ResponseEntity<String> generateKey(HttpServletRequest request, HttpServletResponse response);

    @PostMapping("/key/check_key")
    ResponseEntity checkKey(@RequestParam int key);
}
