package ru.wtll.back.users;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.mail.*;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import java.util.Properties;

@Component
@RestController
@RequestMapping("api/email")
public class EmailController {

    private final String username;
    private final String password;
    private final Properties props;

    public EmailController() {
        this.username = System.getenv("EMAIL");
        this.password = System.getenv("PASSWORD");

        props = new Properties();
        props.put("mail.smtp.auth", "true");
        props.put("mail.smtp.starttls.enable", "true");
        props.put("mail.smtp.host", System.getenv("SMTP_HOST"));
        props.put("mail.smtp.port", System.getenv("SMTP_PORT"));
    }

    @PostMapping("/send")
    public ResponseEntity send(@RequestParam String text, @RequestParam String address) {
        Session session = Session.getInstance(props, new Authenticator() {
            protected PasswordAuthentication getPasswordAuthentication() {
                return new PasswordAuthentication(username, password);
            }
        });
        try {
            Message message = new MimeMessage(session);
            //от кого
            message.setFrom(new InternetAddress(username));
            //кому
            message.setRecipients(Message.RecipientType.TO, InternetAddress.parse(address));
            //Заголовок письма
            message.setSubject("Приглашение стать автором на WTLL!");
            //Содержимое
            message.setText(text);

            //Отправляем сообщение
            Transport.send(message);
            return new ResponseEntity(HttpStatus.ACCEPTED);
        } catch (MessagingException e) {
            return new ResponseEntity(HttpStatus.NOT_FOUND);
        }
    }
}
