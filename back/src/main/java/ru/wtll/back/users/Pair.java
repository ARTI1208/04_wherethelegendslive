package ru.wtll.back.users;

public class Pair<T> {
    private T login;
    private T password;

    public Pair(T login, T password) {
        this.login = login;
        this.password = password;
    }

    public T getLogin() {
        return login;
    }

    public T getPassword() {
        return password;
    }
}
