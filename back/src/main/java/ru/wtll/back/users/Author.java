package ru.wtll.back.users;

import java.util.List;

public class Author {

    private String name;
    private String info;
    private String photoId;
    private List<SocialNetworkLink> socialNetworks;

    public Author() {
    }

    public Author(String name, String info, String photoId, List<SocialNetworkLink> socialNetworks) {
        this.name = name;
        this.info = info;
        this.photoId = photoId;
        this.socialNetworks = socialNetworks;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setInfo(String info) {
        this.info = info;
    }

    public void setPhotoId(String photoId) {
        this.photoId = photoId;
    }

    public void setSocialNetworks(List<SocialNetworkLink> socialNetworks) {
        this.socialNetworks = socialNetworks;
    }

    public String getInfo() {
        return info;
    }

    public String getName() {
        return name;
    }

    public String getPhotoId() {
        return photoId;
    }

    public List<SocialNetworkLink> getSocialNetworks() {
        return socialNetworks;
    }

}
