package ru.wtll.back.users;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.tika.Tika;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.FileSystemResource;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.mail.Multipart;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.sql.DataSource;
import java.io.File;
import java.io.IOException;
import java.nio.file.Path;
import java.util.*;

@Component
@RestController
@RequestMapping("api/users")
public class PostgresDAO implements UserDao, GenerateKeyDAO, IAuthorsAccess {
    private static final Logger logger = LogManager.getRootLogger();

    private JdbcTemplate jdbcTemplate;
    private File photosDirectory = new File(new File(System.getProperty("user.home"), "wtll_data"), "photos");
    Tika fileTypeDetectorr = new Tika();

    @Autowired
    void setJdb(DataSource dataSource) {

        jdbcTemplate = new JdbcTemplate(dataSource);
        String sqlAuthors = "CREATE TABLE IF NOT EXISTS authors(id serial primary key,name text,info text,photo_id text,social_networks text)";
        String sqlCodes = "CREATE TABLE IF NOT EXISTS codes(code int)";
        String sqlUsers = "CREATE TABLE IF NOT EXISTS users(id serial primary key,email text,password text)";
        String sqlSessionCodes = "CREATE TABLE IF NOT EXISTS sessionCodes(id int,code text)";
        jdbcTemplate.execute(sqlAuthors);
        jdbcTemplate.execute(sqlCodes);
        jdbcTemplate.execute(sqlUsers);
        jdbcTemplate.execute(sqlSessionCodes);

    }

    @Override
    public boolean getUserExist(String email) {
        User user = getUserByEmail(email);
        return user != null;
    }

    @Override
    public User getUserByEmail(String email) {
        String sql = "SELECT * FROM users WHERE email=?";
        List<User> user = jdbcTemplate.query(sql, new Object[]{email}, (rs, rowNum) -> new User(rs.getInt("id"), rs.getString("email"), rs.getString("password")));
        if (user.isEmpty())
            return null;
        else return user.get(0);
    }

    public User getUserById(int id) {
        String sql = "SELECT * FROM users WHERE id=?";
        List<User> user = jdbcTemplate.query(sql, new Object[]{id}, (rs, rowNum) -> new User(rs.getInt("id"), rs.getString("email"), rs.getString("password")));
        if (user.isEmpty())
            return null;
        else return user.get(0);
    }

    @Override
    public void insert(User user) {
        String insertString = "INSERT INTO users (id,email,password) VALUES(?,?,?)";
        jdbcTemplate.update(insertString, user.getId(), user.getLogin(), user.getPassword());
    }


    @Override
    @PostMapping("/register")
    public ResponseEntity register(@RequestBody User user) {
        if (user.getLogin().equals("") | user.getPassword().equals("")) {
            logger.error("Неудачная попытка регистрации!Причина:неверные данные.User:" + user.toString());
            return new ResponseEntity<String>(HttpStatus.NOT_ACCEPTABLE);
        }
        if (getUserExist(user.getLogin())) {
            logger.error("Неудачная попытка регистрации!Причина:уже есть такой пользователь.User:" + user.toString());
            return new ResponseEntity(HttpStatus.NOT_FOUND);
        }
        String sql = "SELECT * FROM users";
        List<User> users = jdbcTemplate.query(sql, (rs, rowNum) -> new User(rs.getInt("id"), rs.getString("email"), rs.getString("password")));
        int id = users.size() + 1;
        insert(new User(id, user.getLogin(), user.getPassword()));
        logger.error("Удачная попытка регистрации!User:" + user.toString());
        return new ResponseEntity(HttpStatus.OK);
    }

    @Override
    @PostMapping("/login")
    public ResponseEntity<String> login(@RequestBody User user, HttpServletRequest request, HttpServletResponse response) {
        Cookie[] cookies = request.getCookies();
        if (cookies != null && cookies.length >= 1) {
            if (CheckSession(request).getStatusCode().equals(HttpStatus.OK)) {
                logger.error("Удачный вход!Куки сработали).User:" + user.toString());
                return new ResponseEntity<>("Already logged in", HttpStatus.OK);
            }
        }
        if (!getUserExist(user.getLogin())) {
            logger.error("Неудачная попытка входа!Причина:несуществующий логин.User:" + user.toString());
            return new ResponseEntity<>("User " + user.getLogin() + " not found", HttpStatus.NOT_FOUND);
        }
        User userCur = getUserByEmail(user.getLogin());
        if (user.getPassword().equals(userCur.getPassword())) {
            Cookie cookieId = new Cookie("id", String.valueOf(userCur.getId()));
            Cookie cookieSessionID = new Cookie("sessionId", request.getSession().getId());
            cookieId.setPath("/");
            cookieId.setDomain("wtllfrontend.cfapps.io");
//            cookieId.setSecure(false);
            cookieId.setHttpOnly(true);
            cookieSessionID.setHttpOnly(false);
            cookieSessionID.setDomain(".wtllfrontend.cfapps.io");
            cookieSessionID.setPath("/");

            cookieId.setMaxAge(100000000);
            cookieSessionID.setMaxAge(1000000);

            response.addCookie(cookieSessionID);
            response.addCookie(cookieId);
            logger.error("Удачная попытка входа!Куки сохранены!User:" + user.toString() + " SessionId:" + request.getSession().getId());
            addRandomCode(userCur.getId(), request);
            return new ResponseEntity<>("Cookie save:" + userCur.getId() + " " + request.getSession().getId(), HttpStatus.OK);
        } else {
            logger.error("Неудачная попытка входа!Причина:неверный пароль.User:" + user.toString());
            return new ResponseEntity<>("Invalid password", HttpStatus.NOT_FOUND);
        }

    }

    @Override
    @GetMapping("/key/generate_key")
    public ResponseEntity<String> generateKey(HttpServletRequest request, HttpServletResponse response) {

        Random random = new Random();
        String sql = "SELECT code FROM codes WHERE code=? ";
        boolean duplicate = true;
        int x = -1;
        while (duplicate) {
            x = Math.abs(random.nextInt());
            List<Integer> codes = jdbcTemplate.query(sql, new Object[]{x}, (rs, row) -> rs.getInt("code"));
            if (codes.isEmpty())
                duplicate = false;
        }
        String _sql = "INSERT INTO codes (code) VALUES(?)";
        jdbcTemplate.update(_sql, new Object[]{x});
        return ResponseEntity.status(HttpStatus.OK).body(Integer.toString(x));

    }

    @Override
    @PostMapping("/key/check_key")
    public ResponseEntity checkKey(@RequestParam int key) {
        String sql = "SELECT code FROM codes WHERE code=? ";
        List<Integer> codes = jdbcTemplate.query(sql, new Object[]{key}, (rs, row) -> rs.getInt("code"));
        if (!codes.isEmpty()) {
//            String _sql = "DELETE FROM codes WHERE code=?";
//            jdbcTemplate.update(_sql, new Object[]{key});
            return new ResponseEntity(HttpStatus.OK);
        } else {
            return new ResponseEntity(HttpStatus.NOT_FOUND);
        }
    }

    @Override
    @GetMapping("/authors/all")
    public List<Author> getAllAuthors() {
        String sql = "SELECT * FROM authors";
        List<Author> authors = jdbcTemplate.query(sql, (rs, rowNum) -> {
            String social = rs.getString("social_networks");
            String[] networks = social.split("%");
            ArrayList<SocialNetworkLink> list = new ArrayList<SocialNetworkLink>();
            for (int i = 0; i < networks.length; i++) {
                list.add(new SocialNetworkLink(social.split("!")[0], social.split("!")[1]));
            }
            return new Author(rs.getString("name"), rs.getString("info"), rs.getString("photo_id"), list);
        });
        return authors;
    }

    @Override
    @GetMapping("/authors/author")
    public Author getAuthor(@RequestParam int id) {
        String sql = "SELECT * FROM authors WHERE id=?";
        List<Author> authors = jdbcTemplate.query(sql, new Object[]{id}, (rs, rowNum) -> {
            String social = rs.getString("social_networks");
            String[] networks = social.split("%");
            ArrayList<SocialNetworkLink> list = new ArrayList<SocialNetworkLink>();
            for (int i = 0; i < networks.length; i++) {
                list.add(new SocialNetworkLink(social.split("!")[0], social.split("!")[1]));
            }
            return new Author(rs.getString("name"), rs.getString("info"), rs.getString("photo_id"), list);
        });
        if (authors.size() == 0)
            return null;
        else
            return authors.get(0);
    }

    @Override
    @PostMapping("authors/create")
    public ResponseEntity createAuthor(@RequestParam String name, @RequestParam String info, @RequestParam String photo_id, @RequestParam String socialNetworks) {
        String sql = "INSERT INTO authors(name,info,photo_id,social_networks) VALUES (?,?,?,?)";
        jdbcTemplate.update(sql, name, info, photo_id, socialNetworks);
        logger.error("Создан новый автор!" + name);
        return new ResponseEntity(HttpStatus.OK);
    }

    @PostMapping("update_author")
    public ResponseEntity changeAuthor(@RequestBody Author author, HttpServletRequest request) {
        int id = -1;
        Cookie[] cookies = request.getCookies();
        if (cookies == null)
            return new ResponseEntity("no cookies", HttpStatus.NOT_FOUND);
        for (int i = 0; i < cookies.length; i++)
            if (cookies[i].getName().equals("id"))
                id = Integer.parseInt(cookies[i].getValue());
        if (id == -1)
            return new ResponseEntity("no id", HttpStatus.NOT_FOUND);
        if (changeInfo(id, author.getInfo()) && changeNickname(id, author.getName()) && changeSocialNetworks(id, author.getSocialNetworks()) && changePhoto(id, author.getPhotoId()))
            return new ResponseEntity(HttpStatus.OK);
        else
            return new ResponseEntity("cnnot write", HttpStatus.NOT_FOUND);

    }

    @Override
    @GetMapping("authors/photo")
    public ResponseEntity<FileSystemResource> getPhoto(@RequestParam int id) throws IOException {
        System.out.println(photosDirectory);
        Path file = photosDirectory.toPath().resolve(Integer.toString(id)).resolve("avatar.jpg");
        System.out.println(file);
        String[] files = fileTypeDetectorr.detect(file).split("/");
        return ResponseEntity
                .status(HttpStatus.OK)
                .contentType(new MediaType(files[0], files[1]))
                .body(new FileSystemResource(file));

    }

    @GetMapping("authors/info")
    @Override
    public ResponseEntity<String> getInfo(int id, HttpServletRequest request, HttpServletResponse response) {
        String sql = "SELECT info FROM authors WHERE id=?";
        List<String> info = jdbcTemplate.query(sql, new Object[]{id}, (rs, rowNum) -> rs.getString("info"));
        if (info.isEmpty()) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        } else {
            return ResponseEntity.status(HttpStatus.ACCEPTED).header("Content-Type", "text/plain; charset=utf-8").body(info.get(0));
        }
    }

    private void addRandomCode(int id, HttpServletRequest request) {
        String sql = "SELECT code FROM sessionCodes WHERE id=?";
        List<String> list = jdbcTemplate.query(sql, new Object[]{id}, (rs, rowNum) -> rs.getString("code"));
        if (list == null || list.isEmpty()) {
            String sqlInsert = "INSERT INTO sessionCodes (id,code) VALUES(?,?)";
            jdbcTemplate.update(sqlInsert, id, request.getSession().getId());
        } else {
            String sqlInsert = "UPDATE sessionCodes SET code=? WHERE id =?";
            jdbcTemplate.update(sqlInsert, list.get(0) + ";" + request.getSession(), id);
        }
    }

    @Override
    public ResponseEntity getNickname(int id) {
        String sql = "SELECT name FROM authors WHERE id=?";
        List<String> name = jdbcTemplate.query(sql, new Object[]{id}, (rs, rowNum) -> rs.getString("name"));
        if (name.isEmpty()) {
            return new ResponseEntity(HttpStatus.NOT_FOUND);
        } else {
            return ResponseEntity.status(HttpStatus.ACCEPTED).body(name.get(0));
        }
    }

    @GetMapping("authors/social_networks")
    @Override
    public ResponseEntity<String> getSocialNetworks(int id) {
        String sql = "SELECT social_networks FROM authors WHERE id=?";
        List<String> social_networks = jdbcTemplate.query(sql, new Object[]{id}, (rs, rowNum) -> rs.getString("social_networks"));
        if (social_networks.isEmpty()) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        } else {
            return ResponseEntity.status(HttpStatus.ACCEPTED).header("Content-Type", "text/plain; charset=utf-8").body(social_networks.get(0));
        }
    }

    @Override
    public boolean changeSocialNetworks(int id, List<SocialNetworkLink> socialNetworks) {
        if (getUserById(id) == null)
            return false;
        String compiled = "";
        for (int i = 0; i < socialNetworks.size(); i++) {
            compiled += socialNetworks.get(i).link + "!" + socialNetworks.get(i).title;
            if (i + 1 != socialNetworks.size())
                compiled += "%";
        }
        String sql = "UPDATE authors SET social_networks=? WHERE id=?";
        jdbcTemplate.update(sql, new Object[]{compiled, id});
        return true;
    }


    @Override
    public boolean changeNickname(int id, String nickName) {
        if (getUserById(id) == null)
            return false;
        String sql = "UPDATE authors SET name=? WHERE id=?";
        jdbcTemplate.update(sql, new Object[]{nickName, id});
        return true;

    }


    @Override
    public boolean changeInfo(int id, String info) {
        if (getUserById(id) == null)
            return false;
        String sql = "UPDATE authors SET info=? WHERE id=?";
        jdbcTemplate.update(sql, new Object[]{info, id});
        return true;
    }


    @Override
    public boolean changePhoto(int id, String photo_id) {
        if (getUserById(id) == null)
            return false;
        String sql = "UPDATE authors SET photo_id=? WHERE id=?";
        jdbcTemplate.update(sql, new Object[]{photo_id, id});
        return true;
    }

    @PostMapping("delete_author")
    @Override
    public ResponseEntity deleteAccount(HttpServletRequest request, HttpServletResponse response) {
        Cookie[] cookies = request.getCookies();
        int id = -1;
        if (cookies == null)
            return new ResponseEntity(HttpStatus.NOT_FOUND);
        for (int i = 0; i < cookies.length; i++) {
            if (cookies[i].getName().equals("id"))
                id = Integer.parseInt(cookies[i].getValue());
        }
        if (id == -1)
            return new ResponseEntity(HttpStatus.NOT_FOUND);
        String sqlAuthor = "DELETE FROM authors WHERE id=?";
        String sqlUser = "DELETE FROM users WHERE id=?";
        jdbcTemplate.update(sqlAuthor, new Object[]{id});
        jdbcTemplate.update(sqlUser, new Object[]{id});

        Cookie deleteSessionId = new Cookie("sessionId", "");
        deleteSessionId.setPath("/");
        deleteSessionId.setMaxAge(0);
        Cookie deleteId = new Cookie("id", "");
        deleteId.setPath("/");
        deleteId.setMaxAge(0);
        response.addCookie(deleteId);
        response.addCookie(deleteSessionId);
        return new ResponseEntity(HttpStatus.OK);
    }

    @PostMapping("logout")
    @Override
    public ResponseEntity exitAccount(HttpServletRequest request, HttpServletResponse response) {
        Cookie[] cookies = request.getCookies();
        String sessionId = "null";
        int id = -1;
        if (cookies == null)
            return new ResponseEntity(HttpStatus.NOT_FOUND);
        for (int i = 0; i < cookies.length; i++) {
            if (cookies[i].getName().equals("id"))
                id = Integer.parseInt(cookies[i].getValue());
            if (cookies[i].getName().equals("sessionId"))
                sessionId = cookies[i].getValue();
        }
        if (sessionId.equals("null") || id == -1)
            return new ResponseEntity(HttpStatus.NOT_FOUND);
        Cookie deleteSessionId = new Cookie("sessionId", "");
        deleteSessionId.setPath("/");
        deleteSessionId.setMaxAge(0);
        Cookie deleteId = new Cookie("id", "");
        deleteId.setPath("/");
        deleteId.setMaxAge(0);
        response.addCookie(deleteId);
        response.addCookie(deleteSessionId);
        deleteSessionIdFromDB(id, sessionId);
        return new ResponseEntity(HttpStatus.OK);
    }

    public boolean deleteSessionIdFromDB(int id, String sessionId) {
        String sqlGetCode = "SELECT code FROM sessionCodes WHERE id=?";
        List<String> code = jdbcTemplate.query(sqlGetCode, new Object[]{id}, (rs, rownum) -> rs.getString("code"));
        if (code == null || code.size() == 0)
            return true;
        String format = code.get(0);
        String[] mass = format.split(";");
        String finalString = "";
        for (int i = 0; i < format.split(";").length; i++)
            if (!mass[i].equals(sessionId))
                finalString += mass[i] + ";";
        if (finalString.length() != 0)
            finalString = finalString.substring(0, finalString.length() - 2);
        String sqlInsert = "UPDATE sessionCodes SET code=? WHERE id =?";
        jdbcTemplate.update(sqlInsert, new Object[]{finalString, id});
        return true;
    }

    @PostMapping("save_photo")
    @Override
    public ResponseEntity addPhoto(MultipartFile file, HttpServletRequest request) {
        Cookie[] cookies = request.getCookies();
        if (cookies == null)
            return new ResponseEntity(HttpStatus.NOT_FOUND);
        int id = -1;
        for (int i = 0; i < cookies.length; i++)
            if (cookies[i].getName().equals("id"))
                id = Integer.parseInt(cookies[i].getValue());
        if (id == -1)
            return new ResponseEntity(HttpStatus.NOT_FOUND);
        File fileToDelete = new File(photosDirectory.toPath().resolve(Integer.toString(id)).resolve("avatar.jpg").toUri());
        fileToDelete.delete();
        File fileToSave = new File(photosDirectory.toPath().resolve(Integer.toString(id)).resolve("avatar.jpg").toUri());
        try {
            file.transferTo(fileToSave);
            return new ResponseEntity(HttpStatus.OK);
        } catch (IOException e) {
            return new ResponseEntity(HttpStatus.NOT_FOUND);
        }

    }


    @GetMapping("/validate")
    public ResponseEntity<Integer> CheckSession(HttpServletRequest request) {
        Cookie[] cookies = request.getCookies();
        if (cookies == null)
            return new ResponseEntity<>(-1, HttpStatus.UNAUTHORIZED);
        String _id = "null";
        String _sessionId = "null";
        for (Cookie cookie : cookies) {
            if (cookie.getName().equals("id"))
                _id = cookie.getValue();
            if (cookie.getName().equals("sessionId"))
                _sessionId = cookie.getValue();
        }
        if (_id.equals("null") || _sessionId.equals("null"))
            return new ResponseEntity<>(-1, HttpStatus.UNAUTHORIZED);
        int id = Integer.parseInt(_id);
        String sql = "SELECT code FROM sessionCodes WHERE id=?";
        List<String> code = jdbcTemplate.query(sql, new Object[]{id}, (rs, rowNum) -> rs.getString("code"));
        if (code == null || code.size() == 0)
            return new ResponseEntity<>(-1, HttpStatus.UNAUTHORIZED);
        String[] codes = code.get(0).split(";");
        boolean contain = false;
        for (String s : codes)
            if (s.equals(_sessionId)) {
                contain = true;
                break;
            }
        return new ResponseEntity<>(contain ? id : -1, contain ? HttpStatus.OK : HttpStatus.UNAUTHORIZED);
    }
}
