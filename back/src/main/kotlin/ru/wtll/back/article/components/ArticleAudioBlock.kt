package ru.wtll.back.article.components

import com.fasterxml.jackson.databind.ObjectMapper
import java.io.File
import java.io.FileWriter

class ArticleAudioBlock : ArticleComponent {

    data class Audio(val audio: String, val name: String = "", val description: String = "") {

        fun writeData(componentFolder: File): Boolean {
            FileWriter(File(componentFolder, "data")).use {
                it.write(audio)
            }

            if (name.isNotEmpty()) {
                FileWriter(File(componentFolder, "config")).use {
                    it.write(name)
                }
            }

            return true
        }

    }

    val audios: MutableList<Audio> = mutableListOf()

    override fun writeData(componentFolder: File): Boolean {
        if (!componentFolder.isDirectory) {
            componentFolder.delete()
        }

        if (!componentFolder.exists()) {
            componentFolder.mkdir()
        }

        audios
                .mapIndexed { index, audio -> index to audio }
                .parallelStream()
                .forEach { pair ->
                    val elementFolder = File(componentFolder, pair.first.toString())
                    elementFolder.mkdir()

                    pair.second.writeData(elementFolder)
                }

        ObjectMapper().writeValue(File(componentFolder, "full"), this)

        return true

    }

    override fun asPlainText(): String {
        return audios.joinToString("\n") { it.name }
    }

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as ArticleAudioBlock

        if (audios != other.audios) return false

        return true
    }

    override fun hashCode(): Int {
        return audios.hashCode()
    }

    override val isContainer: Boolean
        get() = true
}