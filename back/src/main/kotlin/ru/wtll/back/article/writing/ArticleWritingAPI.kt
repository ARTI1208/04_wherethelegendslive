package ru.wtll.back.article.writing

import org.apache.logging.log4j.LogManager
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Qualifier
import org.springframework.context.ApplicationContext
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*
import org.springframework.web.multipart.MultipartFile
import ru.wtll.back.article.Article
import ru.wtll.back.users.PostgresDAO
import java.io.File
import java.io.FileOutputStream
import java.security.MessageDigest
import javax.servlet.http.HttpServletRequest
import javax.xml.bind.annotation.adapters.HexBinaryAdapter


@RestController
@RequestMapping("api/article")
class ArticleWritingAPI(var directory: File = Article.articlesDirectory, val withValidation: Boolean = true) {

    private val logger = LogManager.getLogger(ArticleWritingAPI::class.java)

    @Qualifier("postgresDAO")
    @Autowired
    lateinit var dbAPI: PostgresDAO

    @PostMapping("/save")
    fun saveArticle(request: HttpServletRequest, @RequestBody article: Article): ResponseEntity<String> {

        if (withValidation && dbAPI.CheckSession(request).statusCode != HttpStatus.OK) {
            return ResponseEntity
                    .status(HttpStatus.UNAUTHORIZED)
                    .body("You are not authorized to perform this operation")
        }


        if (!directory.exists()) {
            directory.mkdirs()
        }

        val canWrite = directory.exists() && directory.isDirectory
        if (!canWrite) {
            logger.error("Cannot create article dir: exists = ${directory.exists()} && isDir = ${directory.isDirectory}")
            return ResponseEntity
                    .status(HttpStatus.INTERNAL_SERVER_ERROR)
                    .body("Cannot create articles dir!")
        }

        val idCookieValue = request.cookies?.find { it.name == "id" }?.value?.toIntOrNull() ?: 0

        article.config.authorId = idCookieValue

        try {
            article.write(directory
                    .toPath()
                    .resolve(article.config.code)
                    .resolve(article.config.state.toString())
                    .toFile(),
                    mainConfigDirectory = directory.parentFile
            )
        } catch (e: Exception) {
            logger.error("Error during article write: article = $article, error: ${e.message}")
            return ResponseEntity
                    .status(HttpStatus.INTERNAL_SERVER_ERROR)
                    .body(e.message)
        }

        return ResponseEntity.status(HttpStatus.CREATED).build()
    }

    @PostMapping("/delete")
    fun deleteArticle(request: HttpServletRequest, @RequestBody name: String): ResponseEntity<String> {

        if (withValidation && dbAPI.CheckSession(request).statusCode != HttpStatus.OK) {
            return ResponseEntity
                    .status(HttpStatus.UNAUTHORIZED)
                    .body("You are not authorized to perform this operation")
        }

        directory
                .listFiles { file ->
                    file.name == name.hashCode().toString()
                }?.forEach {
                    println(it.deleteRecursively())
                }

        return if (File(directory, name.hashCode().toString()).exists()) {
            logger.error("Error during article delete: article = $name")
            ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Error deleting")
        } else
            ResponseEntity.status(HttpStatus.OK).build()
    }

    @PostMapping("/copy_file")
    fun copyFile(request: HttpServletRequest,
                 @RequestParam articleFrom: String,
                 @RequestParam articleTo: String,
                 @RequestParam fileCode: String): ResponseEntity<String> {

        if (withValidation && dbAPI.CheckSession(request).statusCode != HttpStatus.OK) {
            return ResponseEntity
                    .status(HttpStatus.UNAUTHORIZED)
                    .body("You are not authorized to perform this operation")
        }

        val articleFromHashName = articleFrom.hashCode().toString()
        val articleToHashName = articleTo.hashCode().toString()

        val articleFromFolder = File(directory, articleFromHashName)
        val fromFilesSubFolder = File(articleFromFolder, "files")
        val fileToCopy = File(fromFilesSubFolder, fileCode)

        val articleToFolder = File(directory, articleToHashName)
        val toFilesSubFolder = File(articleToFolder, "files")

        if (!fileToCopy.exists() || (!toFilesSubFolder.exists() && !toFilesSubFolder.mkdirs())) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Data cannot be saved")
        }

        val newFile = File(toFilesSubFolder, fileCode)

        fileToCopy.copyTo(newFile, true)
        return if (!newFile.exists()) {
            logger.error("Error during copying")
            ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Error copying")
        } else {
            logger.debug("Successful copy of $fileCode from $articleFromHashName to $articleToHashName")
            ResponseEntity.status(HttpStatus.OK).body(fileCode)
        }
    }

    @PostMapping("/save_file")
    fun saveFile(request: HttpServletRequest, @RequestParam article: String, @RequestBody file: MultipartFile): ResponseEntity<String> {

        if (withValidation && dbAPI.CheckSession(request).statusCode != HttpStatus.OK) {
            return ResponseEntity
                    .status(HttpStatus.UNAUTHORIZED)
                    .body("You are not authorized to perform this operation")
        }

        val articleHashName = article.hashCode().toString()

        val articleFolder = File(directory, articleHashName)

        val filesSubFolder = File(articleFolder, "files")

        if (!filesSubFolder.exists() && !filesSubFolder.mkdirs()) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Data cannot be saved")
        }

        val fileName = HexBinaryAdapter().marshal(MessageDigest.getInstance("MD5").digest(file.bytes))

        FileOutputStream(File(filesSubFolder, fileName)).use { stream ->
            try {
                stream.write(file.bytes)
            } catch (e: Exception) {
                logger.error("Error during file save: e = ${e.message}")
                return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build()
            }
        }

        return ResponseEntity.status(HttpStatus.CREATED).body(fileName)
    }
}