package ru.wtll.back.article.components

import com.fasterxml.jackson.annotation.JsonTypeInfo
import com.fasterxml.jackson.databind.ObjectMapper
import java.io.File
import java.io.FileWriter

@JsonTypeInfo(
        use = JsonTypeInfo.Id.NAME,
        include = JsonTypeInfo.As.PROPERTY,
        property = "type"
)
class ArticleImageBlock : ArticleComponent {

    data class Image(val image: String, val note: String = "") {

        fun writeData(componentFolder: File): Boolean {
            FileWriter(File(componentFolder, "data")).use {
                it.write(image)
            }

            if (note.isNotEmpty()) {
                FileWriter(File(componentFolder, "config")).use {
                    it.write(note)
                }
            }

            return true
        }

    }

    val images: MutableList<Image> = mutableListOf()

    override fun writeData(componentFolder: File): Boolean {
        if (!componentFolder.isDirectory) {
            componentFolder.delete()
        }

        if (!componentFolder.exists()) {
            componentFolder.mkdir()
        }

        images
                .mapIndexed { index, image -> index to image }
                .parallelStream()
                .forEach { pair ->
                    val elementFolder = File(componentFolder, pair.first.toString())
                    elementFolder.mkdir()

                    pair.second.writeData(elementFolder)
                }

        ObjectMapper().writeValue(File(componentFolder, "full"), this)

        return true
    }

    override fun asPlainText(): String {
        return images.joinToString("\n") { it.note }
    }

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as ArticleImageBlock

        if (images != other.images) return false

        return true
    }

    override fun hashCode(): Int {
        return images.hashCode()
    }

    override val isContainer: Boolean
        get() = true

}