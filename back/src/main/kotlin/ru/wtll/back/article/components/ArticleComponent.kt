package ru.wtll.back.article.components

import com.fasterxml.jackson.annotation.JsonIgnore
import com.fasterxml.jackson.annotation.JsonSubTypes
import com.fasterxml.jackson.annotation.JsonTypeInfo
import com.fasterxml.jackson.module.kotlin.jacksonObjectMapper
import java.io.File

@JsonTypeInfo(
        use = JsonTypeInfo.Id.NAME,
        include = JsonTypeInfo.As.PROPERTY,
        property = "type"
)
@JsonSubTypes(
        JsonSubTypes.Type(value = ArticleTextBlock::class, name = "text"),
        JsonSubTypes.Type(value = ArticleAudioBlock::class, name = "audio"),
        JsonSubTypes.Type(value = ArticleImageBlock::class, name = "image")
)
interface ArticleComponent {

    companion object {

        fun readData(componentFolder: File): List<ArticleComponent> {
            if (!componentFolder.exists() || !componentFolder.isDirectory)
                return emptyList()

            val content = componentFolder.list()
            if (content.isNullOrEmpty())
                return emptyList()

            val list = mutableListOf<ArticleComponent>()

            if (content.contains("full")) {
                list.add(jacksonObjectMapper().readValue(File(componentFolder, "full"), ArticleComponent::class.java))
            }
//
//            content.forEach {
//                if (it.isDirectory)
//                    list.addAll(readData(it))
//                else
//
//            }

            return list
        }
    }

    fun writeData(componentFolder: File): Boolean

    fun asPlainText(): String

    val isContainer: Boolean
        @JsonIgnore
        get
}