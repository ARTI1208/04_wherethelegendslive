package ru.wtll.back.article

import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import com.fasterxml.jackson.module.kotlin.jacksonObjectMapper
import ru.wtll.back.ServerCommon
import ru.wtll.back.article.components.ArticleComponent
import java.io.File
import java.io.FileInputStream
import java.io.FileOutputStream

@JsonIgnoreProperties(ignoreUnknown = true)
class Article(
        val config: ArticleConfig,
        val components: Array<ArticleComponent>,
        val main: Boolean = false
) {

    companion object {
        val articlesDirectory = File(ServerCommon.dataDirectory, "articles")

        const val infoFileFileName = "info"

        fun read(id: String, state: String = "public", withComponents: Boolean = true, directory: File = articlesDirectory): Article? {
            return read(File(directory, id), state, withComponents)
        }

        fun read(articleRoot: File, state: String = "public", withComponents: Boolean = true): Article? {
            val articleTypedRoot = File(articleRoot, state)
            val infoFile = File(articleTypedRoot, infoFileFileName)
            if (!infoFile.exists()) {
                println(1)
                return null
            }

            return try {
                val config = jacksonObjectMapper().readValue(infoFile, ArticleConfig::class.java)
                val files = File(articleTypedRoot, "components").listFiles()?.sortedBy { it.name }

                val isMain = config.state == ArticleConfig.State.PUBLIC
                        && MainManager.getMainArticleOfType(config.type) == config.code

                if (!withComponents || files == null || files.isEmpty())
                    Article(config, emptyArray(), isMain)
                else {
                    val components = files.map { ArticleComponent.readData(it) }.flatten()

                    Article(config, components.toTypedArray(), isMain)
                }
            } catch (e: Exception) {
                e.printStackTrace()
                null
            }
        }

    }


    fun containsText(text: String): Boolean {
        if (text.isEmpty() || components.isEmpty()) {
            return false
        }

        val realText = text.toLowerCase()

        return components.any { it.asPlainText().toLowerCase().contains(realText) }
    }

    private fun writeComponents(parentDir: File = config.articleFolder, parallel: Boolean = true) {
        val componentsFolder = parentDir.toPath().resolve("components").toFile()

        componentsFolder.deleteRecursively()

        if (!componentsFolder.exists())
            componentsFolder.mkdirs()

        components
                .mapIndexed { index, value -> index to value }
                .stream()
                .let {
                    if (parallel)
                        it.parallel()
                    else
                        it
                }
                .forEach { pair ->
                    val file = File(componentsFolder, pair.first.toString())
                    pair.second.writeData(file)
                }

    }

    private fun writeConfig(parentDir: File = config.articleFolder) {
        val file = parentDir.toPath().resolve(infoFileFileName).toFile()
        config.writeToStream(FileOutputStream(file), FileInputStream(file))
    }

    fun write(parentDir: File = config.articleFolder, parallel: Boolean = true, mainConfigDirectory: File = ServerCommon.dataDirectory) {
        parentDir.mkdirs()
        writeConfig(parentDir)
        writeComponents(parentDir, parallel)

        if (main || MainManager.getMainArticleOfType(config.type) == null) {
            MainManager.setMainArticle(this, mainConfigDirectory)
        }
    }

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as Article

        if (config != other.config) return false
        if (!components.contentEquals(other.components)) return false

        return true
    }

    override fun hashCode(): Int {
        var result = config.hashCode()
        result = 31 * result + components.contentHashCode()
        result = 31 * result + main.hashCode()
        return result
    }


}